package com.tobiaswinkler.safariguideapplication;

import java.io.Serializable;

import javax.xml.datatype.Duration;

/**
 * Created by Tobias on 12.04.2017.
 */

public class Safari implements Serializable {

    //private class fields
    private String safariName;
    private String safariDescription;

    //some getter and setter
    public Safari(String safariName, int number) {

        this.safariName = safariName + number;
        this.safariDescription = "somelongdiscriptiontext";

    }

    public void setSafariName(String safariName) {
        this.safariName = safariName;
    }

    public String getSafariName() {
        return this.safariName;
    }

    public String getSafariDiscription() {
        return safariDescription;
    }

}
