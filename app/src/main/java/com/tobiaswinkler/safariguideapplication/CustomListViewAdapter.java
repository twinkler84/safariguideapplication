package com.tobiaswinkler.safariguideapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import java.util.ArrayList;
/**
 * Created by Tobias on 12.04.2017.
 */

public class CustomListViewAdapter extends ArrayAdapter<Safari>{


    private ArrayList<Safari> dataSet;
    Context mContext;

    // lookup cache
    private static class ViewHolder {
        TextView titleName;
        TextView description;
    }

    public CustomListViewAdapter(ArrayList<Safari> data, Context context) {
        super(context, R.layout.item_row, data);
        this.dataSet = data;
        this.mContext=context;

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // Get the data item for this position
        Safari dataSafari = getItem(position);
        // Check if an existing view is being reused, otherwise inflate the view
        ViewHolder viewHolder; // view lookup cache stored in tag

        final View result;

        if (convertView == null) {

            viewHolder = new ViewHolder();
            LayoutInflater inflater = LayoutInflater.from(getContext());
            convertView = inflater.inflate(R.layout.item_row, parent, false);
            viewHolder.titleName = (TextView) convertView.findViewById(R.id.item_row_title);
            viewHolder.description = (TextView) convertView.findViewById(R.id.item_row_description);
            result=convertView;
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
            result=convertView;
        }
        viewHolder.titleName.setText(dataSafari.getSafariName());
        viewHolder.description.setText(dataSafari.getSafariDiscription());
        // Return the completed view to render on screen
        return convertView;
    }


}
