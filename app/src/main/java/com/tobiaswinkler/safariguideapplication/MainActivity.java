package com.tobiaswinkler.safariguideapplication;


import android.content.Context;
import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import static android.R.id.list;

public class MainActivity extends AppCompatActivity {

    private ArrayList<Safari> mySafariObjectList = new ArrayList<>();
    ListView listView;
    private static CustomListViewAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initToolbar();
        initSafariListArray();
        //creating new Listview adapter to handle safari objects inside the list.
        adapter = new CustomListViewAdapter(mySafariObjectList, MainActivity.this);
        //assigning safari_list
        listView = (ListView) findViewById(R.id.safari_list);
        listView.setClickable(true);
        //tell the listView what adapter to use
        listView.setAdapter(adapter);
        //setting up onItemClickListener to each item in the list
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Safari safariObject = adapter.getItem(position);
                Intent detailIntent = new Intent(MainActivity.this, Safari.class).putExtra("Safari", safariObject);
                startActivity(detailIntent);
            }
        });


    }
    //initialize custom toolbar
    private void initToolbar() {
        Toolbar myToolbar = (Toolbar) findViewById(R.id.my_toolbar);
        setSupportActionBar(myToolbar);
        getSupportActionBar().setTitle("");
    }
    //initialize safari object array
    private void initSafariListArray() {
        for (int i = 1; i <= 20; i++) {
            mySafariObjectList.add(new Safari("Safari " , i));
        }
    }

}
